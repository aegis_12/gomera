package gomera

import "cloud.google.com/go/pubsub"
import "datamodel/comcat"

// Gomera es el framework de microservicio
type Gomera interface {
	Subscribe(string, func(*pubsub.Message) error)
	Publish(string) func(comcat.ChangeMessage) error
}
