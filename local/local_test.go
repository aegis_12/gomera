package gomlocal

import (
	"fmt"
	"testing"

	"datamodel/comcat"

	"time"

	"cloud.google.com/go/pubsub"
)

func TestPublishSubscribe(t *testing.T) {

	local, err := NewLocal()
	if err != nil {
		panic(err)
	}

	local.Subscribe("hola", func(msg *pubsub.Message) error {
		fmt.Println(msg)

		return nil
	})

	simpleMessage := comcat.Message{
		Appname: "test1",
	}

	holaSub := local.Publish("hola")

	holaSub(simpleMessage)
	holaSub(simpleMessage)

	time.Sleep(1 * time.Second)
}
