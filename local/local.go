package gomlocal

import (
	"datamodel/comcat"

	"encoding/json"
	"errors"

	"gomera"

	"cloud.google.com/go/pubsub"
)

// Local se usa para hacer tests
type Local struct {
	Channels map[string]chan pubsub.Message
}

// NewLocal crea el google
func NewLocal() (gomera.Gomera, error) {
	return &Local{
		Channels: map[string]chan pubsub.Message{},
	}, nil
}

// Subscribe es la abstraccion para el subscriptor al golang (tiene que recibir un comcat.message no?)
func (l *Local) Subscribe(subName string, f func(*pubsub.Message) error) {

	// mirar si ya existe una subscripcion para este topico, por ahora que solo haya una
	_, ok := l.Channels[subName]
	if ok {
		panic("Should be just one subscripcion per topic")
	}

	channel := make(chan pubsub.Message, 10)
	l.Channels[subName] = channel

	go func() {
		for {
			msg := <-channel
			f(&msg)
		}
	}()
}

// Publish para publicar mensajes
func (l *Local) Publish(s string) func(comcat.ChangeMessage) error {
	return func(msg comcat.ChangeMessage) error {
		obj, ok := l.Channels[s]
		if !ok {
			return errors.New("No existe el topico que buscas")
		}

		data, err := json.Marshal(msg)
		if err != nil {
			return err
		}

		msgpubsub := pubsub.Message{
			Data: data,
		}

		obj <- msgpubsub
		return nil
	}
}
