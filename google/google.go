package google

import (
	"context"
	"datamodel/comcat"
	"fmt"
	"gomera"

	"cloud.google.com/go/pubsub"
	"google.golang.org/api/option"

	"encoding/json"

	netcontext "golang.org/x/net/context"
)

// Google es el cliente de framework
type Google struct {
	PubSubClient *pubsub.Client
}

// NewGoogle crea el google
func NewGoogle(project, auth string) (gomera.Gomera, error) {

	ctx := context.Background()

	pubsubClient, err := pubsub.NewClient(ctx, project, option.WithServiceAccountFile(auth))
	if err != nil {
		return nil, err
	}

	return &Google{pubsubClient}, nil
}

// Subscribe es la abstraccion para el subscriptor al golang
func (g *Google) Subscribe(subName string, f func(*pubsub.Message) error) {

	go func() {

		sub := g.PubSubClient.Subscription(subName)
		sub.ReceiveSettings.MaxOutstandingMessages = 1 // para liminar el numero de mensajes

		err := sub.Receive(context.Background(), func(ctx netcontext.Context, m *pubsub.Message) {

			err := f(m)

			fmt.Println("-- err --")
			fmt.Println(err)

			if err != nil {
				fmt.Println("-- ACK --")
				m.Ack()
			} else {
				fmt.Println(err)
			}

		})

		panic(err)

	}()

	/*
		subscription := g.PubSubClient.Subscription(subName)

		it, err := subscription.Pull(context.Background())
		if err != nil {
			panic(err)
		}
		defer it.Stop()

		for {
			msg, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				panic(err)
			}

			//fmt.Println(msg)

			err = f(msg)
			if err != nil {
				fmt.Println(err)
			} else {
				msg.Done(true)
			}

			//msg.Done(true)
		}
	*/

}

func (g *Google) Publish(topicName string) func(comcat.ChangeMessage) error {

	topic := g.PubSubClient.Topic(topicName)

	return func(msg comcat.ChangeMessage) error {

		data, err := json.Marshal(msg)
		if err != nil {
			return err
		}

		res := topic.Publish(context.Background(), &pubsub.Message{
			Data: data,
		})

		fmt.Println("-- publish result --")
		fmt.Println(res)

		return nil
	}

	return nil
}
